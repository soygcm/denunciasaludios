//
//  FaceController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 24/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit

class FaceController: PostSubController {

    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var happyFaceButton: UIButton!
    @IBOutlet weak var sadFaceButton: UIButton!
    @IBOutlet weak var angryFaceButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        step = "face"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func sadFace(_ sender: AnyObject) {
        
        postController.perception(.Sad)
        performSegue(withIdentifier: "category", sender: self)
        
    }
    
    @IBAction func angryFace(_ sender: AnyObject) {
        
        postController.perception(.Angry)
        performSegue(withIdentifier: "category", sender: self)
        
    }
    
    @IBAction func happyFace(_ sender: AnyObject) {
        
        postController.perception(.Happy)
        performSegue(withIdentifier: "category", sender: self)
        
    }
    
    @IBAction func choseLocation(_ sender: AnyObject) {
        
        postController.postControllerDelegate.postShowMap(true)
        
    }
    
    @IBAction func deleteVideo(_ sender: AnyObject) {
        
        
        navigationController?.popViewController(animated: false)
        
        postController.postControllerDelegate.postDeleteVideo()
        
    }
    

}
