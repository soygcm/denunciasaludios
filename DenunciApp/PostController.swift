//
//  NavigationController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 20/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit
import CoreLocation
import FBSDKShareKit
import FBSDKLoginKit
import AssetsLibrary


//  MARK: - PostControllerDelegate

protocol PostControllerDelegate{
    func postShowMap(_ selectLocation: Bool)
    func postDeleteVideo()
    func postStartCapture()
    func postSaved(_ post:Post)
}

//  MARK: - PostSubController

class PostSubController: UIViewController{
    
    var postController: PostController!
    var step:String!
    
    @IBAction func back(_ sender: AnyObject) {
        
        navigationController?.popViewController(animated: false)
        
    }
    
    
}

//  MARK: - PostController

class PostController: UINavigationController, UINavigationControllerDelegate, PostDelegate, FBSDKSharingDelegate {
    
    var post:Post!
    
    var postControllerDelegate:PostControllerDelegate!
    
    var blurEffectView:UIVisualEffectView!
    
    var locationAddress: String!
    
    var locationButton: UIButton!
    
    var happyFaceButton: UIButton!
    var sadFaceButton: UIButton!
    var angryFaceButton: UIButton!

    
    var assetLibrary:ALAssetsLibrary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        post = Post()
        
        assetLibrary = ALAssetsLibrary()
        
        delegate = self
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        blurEffectView.alpha = 0
        view.addSubview(blurEffectView)
        view.sendSubview(toBack: blurEffectView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Post
    
    func location( _ location: CLLocationCoordinate2D, address: String){
        
        post.location = location
        
        locationAddress = address
        
        if locationButton != nil{
            
            locationButton.setTitle(address, for: UIControlState())
            
            happyFaceButton.isEnabled = true
            sadFaceButton.isEnabled = true
            angryFaceButton.isEnabled = true
        
        }
        
        
    }

    
    func videoURL(_ url:URL){
        post.videoURL = url
    }
    
    func perception(_ perception: Post.PostPerception){
        post.perception = perception
    }
    
    func category(_ category:String){
        post.categoryName = category
    }
    
    func description(_ description:String){
        post.descriptionText = description
    }
    
    func optionsShare(_ fb:Bool, ms:Bool){
        post.shareOnFacebook = fb
        post.sendToMS = ms
    }
    
    var alert = UIAlertController()
    
    func savePost(){
        
//        post.formSendToMS(self)
        
        alert = UIAlertController(title: nil, message: "Publicando...", preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.black
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
        post.delegate = self
        post.save()
        
    }
    
    // MARK: - Blur
    
    func showBlur(){
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            
            self.blurEffectView.alpha = 1
            
        }) 
        
    }
    
    func hideBlur(){
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            
            self.blurEffectView.alpha = 0
            
        }) 
        
    }
    
    
    
    // MARK: - PostDelegate
    
    func postSaved(_ post:Post){
        
        alert.dismiss(animated: true, completion: nil)
        
        let videoURL = post.videoURL
        
        assetLibrary.writeVideoAtPath(toSavedPhotosAlbum: videoURL as URL!) { (url:URL!, error:NSError!) in
            
            post.assetUrl = url
            
            self.popToRootViewController(animated: false)
            
            self.postControllerDelegate.postDeleteVideo()
            
            self.hideBlur()
            
            self.postControllerDelegate.postSaved(post)
            
        }
        
        
        
    }
    
    func shareToFacebook(){
        
        let properties = [
            "og:type":                 "video.other",
            "og:title":                "Sample Video",
            "og:description":          "This is a sample video.",
            "og:url": "https://www.facebook.com/soygcm/videos/10154064683294493/?permPage=1"
            //                "og:url":                  "http://appdenuncia.herokuapp.com/denuncia.html"
        ]
        
        let object = FBSDKShareOpenGraphObject(properties: properties)
        
        let action = FBSDKShareOpenGraphAction()
        action.actionType = "denunciaapp:report"
        action.setObject(object, forKey: "other")
        
        let video = FBSDKShareVideo()
//        video.videoURL = url
        
        //            action.setObject(video, forKey: "fb:video")
        
        
        let contentOG = FBSDKShareOpenGraphContent()
        contentOG.action = action
        contentOG.previewPropertyName = "other"
        
        
        
        
        //            let photo = FBSDKSharePhoto()
        //            photo.
        
        
        
        let content = FBSDKShareVideoContent()
        content.video = video
        
        //            contentOG.
        content.contentURL = URL(string: "https://appdenuncia.herokuapp.com/denuncia.html")
        
        let dialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = contentOG
        //            dialog.mode = .ShareSheet
        dialog.delegate = self
        dialog.show()
    }
    

    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        
        print(error)
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
        
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        
        
    }
    
    
    
    // MARK: - Navigation
    
    func faceController(){
    
        viewControllers.first!.performSegue(withIdentifier: "showFaces", sender: viewControllers.first!)
        
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if let controller = viewController as? PostSubController{
            
            controller.postController = self
            
            if controller.step == "category"{
            
                showBlur()
                
                if let controllerCategory = controller as? CategoryController{
                    controllerCategory.face.setImage(UIImage(named:post.perception.rawValue), for: UIControlState())
                }
                
            }
            
            if controller.step == "description"{
            
                if let controllerDescription = controller as? DescriptionController{
                    controllerDescription.face.setImage(UIImage(named:post.perception.rawValue), for: UIControlState())
                    controllerDescription.categoryImage.image = post.getCategoryImage()
                }
            
            }
            
            
            if controller.step == "face"{
                
                hideBlur()
                
                if let faceController = controller as? FaceController{
                    
                    locationButton = faceController.locationButton
                    
                    happyFaceButton = faceController.happyFaceButton
                    sadFaceButton = faceController.sadFaceButton
                    angryFaceButton = faceController.angryFaceButton
                    
                    if locationAddress != nil{
                        locationButton.setTitle(locationAddress, for: UIControlState())
                        
                        happyFaceButton.isEnabled = true
                        sadFaceButton.isEnabled = true
                        angryFaceButton.isEnabled = true
                    }
                    
                }
                
            }
            
        }
        
    }

}
