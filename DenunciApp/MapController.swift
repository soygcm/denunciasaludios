//
//  MapViewController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 13/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit
import GoogleMaps
import Parse
import CircleProgressBar
import FBSDKLoginKit
import FBSDKCoreKit

// TODO: Persist Data

class MapController: UIViewController, GMSMapViewDelegate, PostDelegate{
    
    var post:Post?
    var mapView : GMSMapView!
    @IBOutlet weak var postMarkerInfo: PostMarkerInfo!
    var posts:[Post] = []
    var markers:[PostMarker] = []
    var openMarker: PostMarker?
    var posting = false
    var endUploadVideo = false

    var location: CLLocationCoordinate2D?{
        
        didSet{
            if mapView != nil{
                mapView.animate( to: GMSCameraPosition.camera(withTarget: location!, zoom: 18))
                getPosts()
            }
        }
    }
    
    var defaultLocation = CLLocationCoordinate2D(latitude: 9.6040002, longitude: -84.1149516)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var camera = GMSCameraPosition()
        
        if location != nil{
            camera = GMSCameraPosition.camera(withTarget: location!, zoom: 18)
            getPosts()
        }else{
            camera = GMSCameraPosition.camera(withTarget: defaultLocation, zoom: 8)

        }
        
        if post != nil{
            camera = GMSCameraPosition.camera(withTarget: post!.location, zoom: 18)
        }
        
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        self.view.addSubview(mapView)
        self.view.sendSubview(toBack: mapView)
        
        postMarkerInfo.center = CGPoint(x: 0.5, y: 1)
        
        
        
        if post != nil && !endUploadVideo{
            
            postingPost()
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - getPost
    
    func getPosts(){
        
        let query = PFQuery(className:"Post")
        query.limit = 1000
        query.whereKeyExists("location")
        query.whereKey( "location", nearGeoPoint: PFGeoPoint(latitude: location!.latitude, longitude: location!.longitude) )
        query.whereKey("active", equalTo: true)
        
        query.findObjectsInBackground { (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                
                if let objects = objects {
                    
                    for object in objects {
                        
                        let postMarker = PostMarker( post: Post(pObject: object))
                        
                        postMarker.map = self.mapView
//                        postMarker.hashCompare = spaceMarker.hash
                        
                        self.markers.append(postMarker)
                        
                        
                    }
                }
                
                
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
            
        }
        
        
    }
    
    // MARK: - Posting
    
    func postingPost(){
        posting = true
        
        let postMarker = PostMarker( post: post!)
        postMarker.map = self.mapView
        self.markers.append(postMarker)
        
        openMarker = postMarker
        openMarker!.open = true
        
        postMarkerInfo.marker = openMarker
        postMarkerInfo.show()
        postMarkerInfo.descriptionLabel.text = postMarker.post.descriptionText
        postMarkerInfo.iconCategory.image = postMarker.post.getCategoryImage()
        postMarkerInfo.iconMarker.image = UIImage(named: postMarker.post.perception.rawValue)
        postMarkerInfo.iconMarkerCircle.image = postMarker.post.getMarker()
        
        postMarkerInfo.setPosting(true)
        
        updatePositionMarkerInfo(postMarker.position)
        
//        print(post?.videoURL)
//        print("Start to upload video")
        
        post!.delegate = self
        post!.uploadVideo()
        
    }
    
    func postUploadProgress(_ progress: CGFloat) {
        
        postMarkerInfo.postUploadProgress(progress)
        
    }
    
    func postVideoUploaded() {
        
        endUploadVideo = true
        
        
        post!.active = true
        post!.save()
        
    }
    
    func postVideoUploadedFail() {

        view.makeToast("Sucedió un error al intentar subir tu video.")
        
        posting = false
        
        postMarkerInfo.setPosting(false)
        
        postMarkerInfo.marker = openMarker
        postMarkerInfo.hide()
        openMarker!.hideIcon()
        openMarker!.open = false
        
    }
    
    func postSaved(_ post: Post) {
        
        posting = false
        
        
        postMarkerInfo.setPosting(false)

        postMarkerInfo.marker = openMarker
        postMarkerInfo.hide()
        openMarker!.open = false
        
        self.post?.uploadFacebook(self)
        
        self.post = nil
        
    }
    
    
    func postedOnFacebookFail(_ post: Post) {
        
        if post.shareOnFacebook{
            view.makeToast("Sucedio un error mientras se publicaba el video en tu Facebook.")
        }
        
        post.formSendToMS(self)
        
    }
    
    func postedOnFacebook(_ post: Post) {
        
        if post.shareOnFacebook{
            view.makeToast("El Video se ha publicado en tu muro de Facebook.")
        }
        
        post.formSendToMS(self)
        
    }
    
    func sendedToMS(_ post: Post) {
        
        self.post = nil
        
        if post.sendToMS{
            view.makeToast("Tu denuncia ha sido enviada con éxito")
        }
        
    }
    
    // MARK: - IBActions
    
    @IBAction func showPost(_ sender: AnyObject) {
        
        if !posting{
            postMarkerInfo.marker = openMarker
            postMarkerInfo.hide()
            performSegue(withIdentifier: "showPost", sender: self)
        }
        
        
    }
    
    
    // MARK: - MapDelegate
    
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        if openMarker != nil && !posting{
            postMarkerInfo.hide()
            openMarker!.open = false
        }
        
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        if openMarker != nil && openMarker!.open{
            
            updatePositionMarkerInfo(openMarker!.position)
            
        }
        
    }
    
    func updatePositionMarkerInfo(_ coordinate:CLLocationCoordinate2D){
        let point = mapView.projection.point(for: openMarker!.position)
        
        print(point)
        
        let origin = CGPoint(x: point.x - postMarkerInfo.frame.width/2, y: point.y - postMarkerInfo.frame.height - 22)
        
        var frame = postMarkerInfo.frame
        frame.origin = origin
        postMarkerInfo.frame = frame
        postMarkerInfo.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if !posting{
            
            if openMarker != nil{
                openMarker!.showIcon()
            }
            
            let postMarker = marker as! PostMarker
            
            openMarker = postMarker
            openMarker!.open = true
            postMarkerInfo.marker = openMarker
            postMarkerInfo.show()
            postMarkerInfo.descriptionLabel.text = postMarker.post.descriptionText
            
            postMarkerInfo.iconCategory.image = postMarker.post.getCategoryImage()
            postMarkerInfo.iconMarker.image = UIImage(named: postMarker.post.perception.rawValue)
            postMarkerInfo.iconMarkerCircle.image = postMarker.post.getMarker()
            
            updatePositionMarkerInfo(postMarker.position)
            
            let toLocation = CLLocationCoordinate2D(latitude: marker.position.latitude+0.0005, longitude: marker.position.longitude)
            
            mapView.animate(toLocation: toLocation)
            
        }
        
        
        
        
        return true
    }
    
    // MARK: - Navigation
    
    @IBAction func back(_ sender: AnyObject) {
        
        if !posting {
            
            navigationController?.popViewController(animated: true)
        }
        
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let segueName = segue.identifier

        if (segueName == "showPost") {
            if let controller = segue.destination as? ViewPostController{
                
                controller.post = openMarker!.post
                
            }
        }
    }
    

}


