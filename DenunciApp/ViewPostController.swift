//
//  ViewPostController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 27/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit
import Player
import CircleProgressBar

class ViewPostController: UIViewController , PostDelegate, PlayerDelegate{
    
    var post:Post!
    
    @IBOutlet weak var navigationView: UIView!
    
    @IBOutlet weak var descriptionView: UIView!
    
    @IBOutlet weak var categoryImage: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var faceImage: UIImageView!
    
    var player:Player!
    
    @IBOutlet weak var circleProgressBar: CircleProgressBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rotate = CGAffineTransform(rotationAngle: CGFloat(M_PI)/2)
        
        descriptionLabel.text = post.descriptionText
        faceImage.image = UIImage(named: post.perception.rawValue)
        categoryImage.image = post.getCategoryImage()
        
        self.player = Player()
        
        if post.os != "android" {
            self.player.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.height, height: self.view.frame.width)
            self.player.view.center = self.view.convert(self.view.center, from: self.view.superview)
            self.player.view.transform = rotate
        }else{
            self.player.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }
        
        //        self.player
        self.player.view.backgroundColor = UIColor.black
        self.player.playbackLoops = true
        self.player.fillMode = "AVLayerVideoGravityResizeAspectFill"
        
        post.delegate = self
        post.downloadVideo()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchPlayer))
        self.player.view.addGestureRecognizer(gestureRecognizer)
    }
    
    var fullscreen = false
    
    func touchPlayer(_ gestureRecognizer: UIGestureRecognizer) {
        
        
        let up = CGAffineTransform(translationX: 0, y: -descriptionView.frame.height)
        let down = CGAffineTransform(translationX: 0, y: navigationView.frame.height)
        
        UIView.animate(withDuration: 0.3, animations: { 
            
            if self.fullscreen{
                self.navigationView.transform = CGAffineTransform.identity
                self.descriptionView.transform = CGAffineTransform.identity
            }else{
                self.navigationView.transform = down
                self.descriptionView.transform = up
            }
            
        }) 
        
        fullscreen = !fullscreen
        
    }
    
    //MARK: - PostDelegate
    
    func postVideoDownloaded(_ localUrl: URL) {
        
        self.player.setUrl(localUrl)
        self.player.playFromBeginning()
        self.player.delegate = self
        circleProgressBar.isHidden = true
    }
    
    func postDownloadProgress(_ progress: CGFloat) {
        
        circleProgressBar.setProgress(progress, animated: true)   
    }
    
    func playerReady(_ player: Player) {
        
        self.view.addSubview(self.player.view)
        self.view.sendSubview(toBack: self.player.view)
    }
    
    func playerPlaybackStateDidChange(_ player: Player){}
    func playerBufferingStateDidChange(_ player: Player){}
    
    func playerPlaybackWillStartFromBeginning(_ player: Player){}
    func playerPlaybackDidEnd(_ player: Player){}

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Navigation
    
    @IBAction func back(_ sender: AnyObject) {
        
        post.downloadVideo()
        post.stopDownload()

        navigationController?.popViewController(animated: true)
        
    }
    
    
}
