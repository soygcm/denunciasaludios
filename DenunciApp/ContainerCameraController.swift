//
//  ContainerCameraController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 20/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit
import PBJVision
import Player
import GoogleMaps
import AlertOnboarding

class ContainerCameraController: UIViewController {
    
    var placesClient: GMSPlacesClient?
    
    var previewView:UIView!
    var previewLayer:AVCaptureVideoPreviewLayer!
    @IBOutlet weak var containerView: UIView!
    var currentVideo: [AnyHashable: Any]?
    var player:Player!
    var urlVideo:URL?
    
    var mapController: MapController?
    
    var postController:PostController!
    
    var place:GMSPlace?
    
    var defaultLocation = CLLocationCoordinate2D(latitude: 9.6040002, longitude: -84.1149516)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        
        if launchedBefore  {
            print("Not first launch.")
        }
            
        else {
            print("First launch, setting NSUserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            
            //First, declare datas
            let arrayOfImage = ["logo_ms","ANavegacion", "AGrabar", "ACaras", "ACategorias","ACompartir", "AMapa"]
            let arrayOfTitle = ["DENUNCIA SALUD","NAVEGA FACILMENTE", "VIDEO","PERCEPCIÓN", "CATEGORÍAS","HACER OFICIAL","EN MAPA"]
            let arrayOfDescription = ["Esta aplicación es un esfuerzo del Ministerio de Salud para facilitar el proceso de denuncia de Salud Pública a los ciudadanos y el seguimiento de las mismas.", "Navega fácilmente al utilizar las flechas atrás y adelante. Finaliza tu denuncia al usar el Check." , "Graba un video de 5 segundos para registrar tu denuncia","¿Cuál es tu percepción de la denuncia?. Selecciona la cara que exprese tu percepción. Ubica tu denuncia ubicándola en un mapa","Selecciona una categoría para saber más de ésta, y asi elegir la mejor para tu denuncia","Activa compartir en Facebook para que el video y tu comentario sea publicado en tu muro. Activa el enviar a las autoridades para que tu denuncia sea tramitada como una oficial.", "Verás ubicadas las denuncias hechas por ti y por todas las personas"]
            
            //Simply call AlertOnboarding...
            let alertView = AlertOnboarding(arrayOfImage: arrayOfImage, arrayOfTitle: arrayOfTitle, arrayOfDescription: arrayOfDescription)
            
           
            
            //Modify labels
            alertView.titleGotItButton = "COMENCEMOS"
            alertView.titleSkipButton = " "
            
            //... and show it !
            alertView.show()
            
        }
        
        
        placesClient = GMSPlacesClient()
        getLocation()
        
        let rotate = CGAffineTransform(rotationAngle: CGFloat(M_PI)/2)
        
        previewView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.height, height: self.view.frame.width))
        previewView.center = self.view.convert(self.view.center, from: self.view.superview)
        previewView.transform = rotate
        previewView.backgroundColor = UIColor.black
        
        
        previewLayer = PBJVision.sharedInstance().previewLayer
        previewLayer.frame = previewView.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewView.layer.addSublayer(previewLayer)
        
        
        let vision = PBJVision.sharedInstance()
        vision.delegate = self
        vision.cameraMode = .video
//        vision.vid
        vision.cameraOrientation = .landscapeRight
        vision.focusMode = .autoFocus
        vision.outputFormat = .widescreen
        vision.cameraDevice = .back
//        vision.startPreview()
        vision.maximumCaptureDuration = CMTimeMakeWithSeconds(5, 600)
        
        postController.postControllerDelegate = self
        
        

        
        self.player = Player()
        self.player.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.height, height: self.view.frame.width)
        self.player.view.center = self.view.convert(self.view.center, from: self.view.superview)
        self.player.view.transform = rotate
//        self.player
        self.player.view.backgroundColor = UIColor.black
        self.player.playbackLoops = true
        self.player.fillMode = "AVLayerVideoGravityResizeAspectFill"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        PBJVision.sharedInstance().startPreview()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        PBJVision.sharedInstance().stopPreview()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Location
    
    func getLocation(){
        
        placesClient?.currentPlaceWithCallback({
            (placeLikelihoodList: GMSPlaceLikelihoodList?, error: NSError?) -> Void in
            
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                self.place = placeLikelihoodList.likelihoods.first?.place
                if let place = self.place {
                    
                    self.postController.location(place.coordinate, address: place.formattedAddress != nil ? "\(place.name), \(place.formattedAddress!)" : place.name)
                    
                    if self.mapController != nil{
                        self.mapController!.location = place.coordinate
                    }
                    
                }
            }
            
        })
        
    }
    
    func showPlacePicker(){
        var center = CLLocationCoordinate2D()
        var northEast = CLLocationCoordinate2D()
        var southWest = CLLocationCoordinate2D()
        
        if let place = place{
            center = place.coordinate
            northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
            southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        }else{
            center = defaultLocation
            northEast = CLLocationCoordinate2DMake(center.latitude + 1, center.longitude + 1)
            southWest = CLLocationCoordinate2DMake(center.latitude - 1, center.longitude - 1)
        }
        
        
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        
        placePicker.pickPlaceWithCallback({ (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                
                self.postController.location(place.coordinate, address: place.formattedAddress != nil ? "\(place.name), \(place.formattedAddress!)" : place.name)
                
            } else {
                print("No place selected")
            }
        })
    }
    
    
    // MARK: - CAMERA/PLAYER
    
    func viewPlayer(_ url: URL){
        
        self.player.setUrl(url)
        self.player.playFromBeginning()
        self.view.addSubview(self.player.view)
        self.view.sendSubview(toBack: self.player.view)
        
        PBJVision.sharedInstance().stopPreview()
        
        self.previewView.removeFromSuperview()
        
    }
    
    func viewCameraPreview(){
        
        self.player.view.removeFromSuperview()
        PBJVision.sharedInstance().startPreview()
        
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        let segueName = segue.identifier
        if (segueName == "postController") {
            if let controller = segue.destination as? PostController{
                postController = controller
            }
        }
        if (segueName == "showMap") {
            if let controller = segue.destination as? MapController{
                
                mapController = controller
                
                if let post = sender as? Post{
                    controller.post = post
                    
                }
                if let place = place{
                    controller.location = place.coordinate
                }
                
            }
        }
    }
    
    
    

}

// MARK: - PBJVisionDelegate

extension ContainerCameraController: PBJVisionDelegate{
    
    
    func visionSessionDidStart(_ vision: PBJVision) {
        
        if previewView.superview == nil {
            self.view.addSubview(previewView)
            self.view.sendSubview(toBack: previewView)
        }
        
        
        
    }
    
    func vision(_ vision: PBJVision, capturedVideo videoDict: [AnyHashable: Any]?, error: NSError?) {
        
        currentVideo = videoDict
        if let videoPath = currentVideo![PBJVisionVideoPathKey] as? String{
            
            urlVideo = URL(fileURLWithPath: videoPath)
            postController.videoURL(urlVideo!)
            
            viewPlayer(urlVideo!)
            postController.faceController()
            
        }
        
    }

}

// MARK: - PostController

extension ContainerCameraController: PostControllerDelegate{

    func postShowMap(_ selectLocation: Bool) {

        if !selectLocation {
        
            performSegue(withIdentifier: "showMap", sender: self)
        
        }else {
            
            showPlacePicker()
            
        }
        
        
    }
    
    func postDeleteVideo() {
        
        viewCameraPreview()
        
        
    }
    
    func postStartCapture(){
        
//        postController.post.formSendToMS(self)
        
        PBJVision.sharedInstance().startVideoCapture()
        
        
    }
    
    
    func postSaved(_ post:Post){
        
        //
        
        performSegue(withIdentifier: "showMap", sender: post)
        
    }
    
}
