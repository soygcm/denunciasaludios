//
//  DescriptionController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 20/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit


class DescriptionController: PostSubController, UITextViewDelegate {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var face: UIButton!
    @IBOutlet weak var categoryImage: UIImageView!
    
    @IBOutlet weak var switchFacebook: UISwitch!
    @IBOutlet weak var switchSendMS: UISwitch!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        step = "description"
        
        descriptionTextView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(DescriptionController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DescriptionController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let userDefaults = UserDefaults.standard
        
        if let switchFB = userDefaults.value(forKey: "switchFB") as? Bool {
            switchFacebook.isOn = switchFB
        }
        
        if let switchMS = userDefaults.value(forKey: "switchMS") as? Bool {
            switchSendMS.isOn = switchMS
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        descriptionTextView.becomeFirstResponder()
        
    }
    
    @IBOutlet weak var heightKeyboard: NSLayoutConstraint!
    
    // MARK: - TextViewDelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn  range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            textView.resignFirstResponder()
            savePost(textView)
        }
        
        return true
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func savePost(_ sender: AnyObject) {
        
        let switchFB = switchFacebook.isOn
        let switchMS = switchSendMS.isOn
        
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(switchFB, forKey: "switchFB")
        userDefaults.setValue(switchMS, forKey: "switchMS")
        userDefaults.synchronize()
        
        postController.optionsShare(switchFB, ms: switchMS)
        
        postController.description(descriptionTextView.text)
        postController.savePost()
        
    }
    
    func keyboardWillShow(_ notification: Notification) {
        keyboardShowOrHide(notification)
    }
    
    func keyboardWillHide(_ notification: Notification) {
        keyboardShowOrHide(notification)
    }
    
    fileprivate func keyboardShowOrHide(_ notification: Notification) {
        guard let userInfo = (notification as NSNotification).userInfo else {return}
        guard let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey]else { return }
        guard let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] else { return }
        guard let keyboardFrameEnd = userInfo[UIKeyboardFrameEndUserInfoKey] else { return }
        
        let curveOption = UIViewAnimationOptions(rawValue: UInt((curve as AnyObject).intValue << 16))
        let keyboardFrameEndRectFromView = view.convert((keyboardFrameEnd as AnyObject).cgRectValue, from: nil)
        UIView.animate(withDuration: (duration as AnyObject).doubleValue ?? 1.0,
                                   delay: 0,
                                   options: [curveOption, .beginFromCurrentState],
                                   animations: { () -> Void in
            
            self.heightKeyboard.constant = keyboardFrameEndRectFromView.size.height
                                    
            }, completion: nil)
    }
    

}
