//
//  Models.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 20/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import Parse
import AWSS3
import FBSDKCoreKit
import FBSDKLoginKit
import Toast_Swift
import Eureka

class FormController : FormViewController,  PostDelegate{
    
    var done: UIBarButtonItem!
    var post:Post!
    var vcPrev: MapController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Enviar denuncia"
        
        form +++ Section("Datos personales")
            <<< NameRow("nombre"){ $0.title = "Nombre" }
            <<< EmailRow("correo"){ $0.title = "Correo" }
            <<< PhoneRow("telefono"){ $0.title = "Teléfono" }
            <<< SwitchRow("confidential"){
                $0.title = "Confidencial"
                $0.value = false
        }
        
        done =  UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(dones))
        navigationItem.rightBarButtonItem = done
        
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: navigationController, action: nil)
        navigationItem.leftBarButtonItem = backButton
        self.navigationItem.setHidesBackButton(true, animated: false)


    }
    
    func validate()-> Bool{
        
        var valid = true
        
        let valuesDictionary = form.values()
        
        if (valuesDictionary["nombre"] as? String) == nil{
            valid = false
        }
        
        if (valuesDictionary["telefono"] as? String) == nil{
            valid = false
        }
        
        if (valuesDictionary["correo"] as? String) == nil{
            valid = false
        }
        
        return valid
        
    }
    
    var alert: UIAlertController!
    
    func dones(){
        
        if validate(){
            
            self.done.isEnabled = false
            
            let valuesDictionary = form.values()

            post.name = valuesDictionary["nombre"] as? String
            post.phone = valuesDictionary["telefono"] as? String
            post.mail = valuesDictionary["correo"] as? String
            post.confidential = valuesDictionary["confidential"] as! Bool
            post.delegate = self
            
            alert = UIAlertController(title: nil, message: "Enviando al Ministerio de Salud...", preferredStyle: .alert)
            
            alert.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            loadingIndicator.startAnimating()
            
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
            
            post.save()
            
        }else{
            
            view.makeToast("No puedes dejar ningun campo vacio")
            
        }
        
        
    }
    
    func postSaved(_ post: Post) {
        
        self.done.isEnabled = true
        
        alert.dismiss(animated: true, completion: {
        
            self.dismiss(animated: true, completion: {});
            
            self.navigationController?.popViewController(animated: true)
            
            self.post.delegate = self.vcPrev
            
            self.post.delegate.sendedToMS!(self.post)
        
        })
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(false, animated: animated)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    
}



@objc protocol PostDelegate{
    
    @objc optional func postUploadProgress(_ progress: CGFloat)
    @objc optional func postDownloadProgress(_ progress: CGFloat)
//    func errorParseSaved()
    @objc optional func postSaved(_ post:Post)
    @objc optional func postVideoUploaded()
    @objc optional func postVideoUploadedFail()
    @objc optional func postVideoDownloaded(_ localUrl:URL)
    @objc optional func postedOnFacebook(_ post:Post)
    @objc optional func postedOnFacebookFail(_ post:Post)
    @objc optional func sendedToMS(_ post:Post)
    
}

class Post:NSObject{
    
    var videoURL:URL!
    var assetUrl: URL?
    var s3Url: URL?
    var tempUrl: URL?
//    var category:Category!
    var descriptionText:String!
    var location:CLLocationCoordinate2D!
    var perception = PostPerception.Sad
    var categoryName:String!
    var parseObject:PFObject!
    var objectId:String?
    var active = false
    var vc:UIViewController?
    
    var os:String?
    
    var name:String?
    var phone:String?
    var mail:String?
    var confidential = false
    
    var shareOnFacebook = true
    var sendToMS = true
    
    var imageRedMarker = UIImage(named: "marcadorRojo")!
    var imageYellowMarker = UIImage(named: "marcadorAmarillo")!
    var imageGreenMarker = UIImage(named: "marcadorVerde")!
    
    static let images:[String:[UIImage]] = [
        "sad":[
            UIImage(named:"mosquitosAmarillo")!,
            UIImage(named:"ruidoAmarillo")!,
            UIImage(named:"aguaAmarillo")!,
            UIImage(named:"oloresAmarillo")!,
            UIImage(named:"residuosAmarillo")!,
            UIImage(named:"humoAmarillo")!,
            UIImage(named:"vectoresAmarillo")!,
            UIImage(named:"atencionAmarillo")!,
            UIImage(named:"restauranteAmarillo")!,
            UIImage(named:"productosAmarillo")!,
            UIImage(named:"tabacoAmarillo")!,
            UIImage(named:"otrosAmarillo")!
        ],
        "happy":[
            UIImage(named:"mosquitosVerde")!,
            UIImage(named:"ruidoVerde")!,
            UIImage(named:"aguaVerde")!,
            UIImage(named:"oloresVerde")!,
            UIImage(named:"residuosVerde")!,
            UIImage(named:"humoVerde")!,
            UIImage(named:"vectoresVerde")!,
            UIImage(named:"atencionVerde")!,
            UIImage(named:"restauranteVerde")!,
            UIImage(named:"productosVerde")!,
            UIImage(named:"tabacoVerde")!,
            UIImage(named:"otrosVerde")!
        ],
        "angry":[
            UIImage(named:"mosquitosRojo")!,
            UIImage(named:"ruidoRojo")!,
            UIImage(named:"aguaRojo")!,
            UIImage(named:"oloresRojo")!,
            UIImage(named:"residuosRojo")!,
            UIImage(named:"humoRojo")!,
            UIImage(named:"vectoresRojo")!,
            UIImage(named:"atencionRojo")!,
            UIImage(named:"restauranteRojo")!,
            UIImage(named:"productosRojo")!,
            UIImage(named:"tabacoRojo")!,
            UIImage(named:"otrosRojo")!
        ]
        
    ]
    
    static let descriptions = [
        //Zika
        "Los criaderos de mosquitos transmisores de dengue, Zika, Chicungunya,  se encuentran en recipientes con agua estancada.",
        //Ruido
        "Hogares, establecimientos, construcciones, etc. Pueden generar sonidos o ruidos fuertes y en horas inadecuadas, perjudicando la salud.",
        //Aguas
        "Las aguas negras, jabonosas, de lluvia, tienen su lugar, de lo contrario pueden afectar el ambiente y la salud humana.",
        //Olores
        "Desagües, basura, sustancias peligrosas, etc. Pueden generar olores que llegan a ser molestos, e indicar un potencial peligro para la salud.",
        //Residuos
        "Los conocemos como Basura. Cualquier desecho (liquido o solido) que esté en un lugar inadecuado, es peligroso para la salud y el ambiente.",
        //Humos
        "Restaurantes, empresas, industrias, con frecuencia emiten humos o gases de forma que impacta viviendas o poblaciones.",
        //Vectores
        "Moscas, cucarachas, ratones, etc. son un peligro para la salud, ya que transmiten enfermedades. (Restaurantes, o de forma excesiva)",
        //Atención
        "La atención al usuario es importante por parte de todo profesional en salud, ya sea de la CCSS, del Ministerio de Salud, o clínica privada.",
        // Restaurantes
        "Los restaurantes deben presentar condiciones apropiadas para distribuir comidas: estar sin plagas, tener agua potable, y los permisos necesarios.",
        // Productos
        "Un producto alterado, deteriorado, contaminado, adulterado o falsificado es un peligro para la salud pública.",
        // Tabaco
        "En restaurantes, bares, parqueos, paradas de buses/ taxis, parques, es prohibido fumar. No se debe vender cigarrillo suelto ni cajas con menos de 20 unidades.",
        // Otros
        "Algunas denuncias no son fáciles de clasificar, después puedes describir detalladamente esta denuncia."
    ]
    
    static let categories = [
        "mosquitos",
        "ruido",
        "agua",
        "olores",
        "residuos",
        "humo",
        "vectores",
        "atencion",
        "restaurante",
        "productos",
        "tabaco",
        "otros"
    ]
    
    //  address
    
    enum PostPerception: String{
        case Sad = "sad"
        case Happy = "happy"
        case Angry = "angry"
        
//        static let values = [Sad, Happy, Angry]
        
    }
    
    var delegate:PostDelegate!
    
    override init() {
        parseObject = PFObject(className:"Post")
    }
    
    init(pObject: PFObject) {
        
        parseObject = pObject
        
        categoryName = pObject["categoryName"] as? String
        descriptionText = pObject["description"] as? String
        
        os = pObject["os"] as? String
        
        if let perceptionString = pObject["perception"] as? String{
            if let perception = PostPerception.init(rawValue:  perceptionString){
                self.perception = perception
            }
        }
        
        if let geoPoint = pObject["location"] as? PFGeoPoint{
            location = CLLocationCoordinate2D(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
        }
        
        
        if let id = parseObject.objectId {
            self.objectId = id
        }
        
    }
    
    func getCategoryImage() -> UIImage!{
        let imageCategory = UIImage(named: "\(categoryName)\(getColorString())")
        return imageCategory
    }
    
    func getMarker()->UIImage{
    
        switch perception {
        case .Sad:  return imageYellowMarker
        case .Happy: return imageGreenMarker
        case .Angry: return imageRedMarker
        }
    
    }
    
    func getColorString() -> String{
        if(perception == .Angry){
            return "Rojo"
        }
        if(perception == .Happy){
            return "Verde"
        }
        if(perception == .Sad){
            return "Amarillo"
        }
        return ""
    }
    
    func save(){
        
        parseObject["categoryName"] = categoryName != nil ? categoryName : ""
        parseObject["description"] = descriptionText != nil ? descriptionText : ""
        parseObject["perception"] = perception.rawValue
        
        parseObject["active"] = active
        
        parseObject["fb"] = shareOnFacebook
        parseObject["ms"] = sendToMS
        
        parseObject["name"] = name != nil ? name : ""
        parseObject["phone"] = phone != nil ? phone : ""
        parseObject["email"] = mail != nil ? mail : ""
        parseObject["confidential"] = confidential
        
        if location != nil {
            parseObject["location"] =  PFGeoPoint(location: CLLocation(latitude: location.latitude, longitude: location.longitude))
        }
        
        parseObject.saveInBackground{ (success: Bool, error: NSError?) -> Void in
            if (success) {
                
                print("savedOnParse \(self.parseObject.objectId)")
                
                self.objectId = self.parseObject.objectId
                
                self.delegate.postSaved!(self)
                
            } else {
                //TODO: catching error
            }
        }
        
    }
    
    func uploadVideo(){
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest.bucket = "appdenuncia"
        
        if let objectId = parseObject.objectId {
            uploadRequest.key = "\(objectId).mp4"
        }
        
        uploadRequest.body = videoURL
        uploadRequest.ACL = .PublicRead
        
        uploadRequest.uploadProgress = { (bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                let percent = CGFloat(totalBytesSent) / CGFloat( totalBytesExpectedToSend)
                
                print(percent)
                
                self.delegate.postUploadProgress!(percent)
                
            })
            
        }
        
        
        transferManager.upload(uploadRequest).continueWithSuccessBlock({ (task: AWSTask) -> AnyObject? in
        
            if task.error != nil{
                print("Error: \( task.error)")
                
                self.delegate.postVideoUploadedFail!()
                
            }
            
            if task.result != nil {
                
//                let uploadOutput = task.result
                
                self.s3Url = NSURL(string: "https://d2vpm3nruwfvqw.cloudfront.net/\(self.objectId!).mp4")
                
                print("video saved")
                self.delegate.postVideoUploaded!()
                
            }
            
            return nil
            
        })
        
    }
    
    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
    
    
    func stopDownload(){
        
        transferManager.cancelAll()
        
    }
    
    func downloadVideo(){
        
        if self.tempUrl != nil {
            self.delegate.postVideoDownloaded!(self.tempUrl!)
        }else{
        
        
        let downloadingFileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("\(objectId!).mp4")
        
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        
        downloadRequest.bucket = "appdenuncia"
        downloadRequest.key = "\(objectId!).mp4"
        downloadRequest.downloadingFileURL = downloadingFileURL
        
        downloadRequest.downloadProgress = { (bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) -> Void in
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                let percent = CGFloat(totalBytesSent) / CGFloat( totalBytesExpectedToSend)
                
                print(percent)
                
                if self.delegate != nil && self.delegate.postDownloadProgress != nil{
                    self.delegate.postDownloadProgress!(percent)
                }
                
            })
            
        }
        
        transferManager.download(downloadRequest).continueWithSuccessBlock({ (task: AWSTask) -> AnyObject? in
            
            if task.error != nil {
                print("Error: \( task.error)")
            }
            
            if task.result != nil {
                // let uploadOutput = task.result
                
                
                print("video download")
                
                self.tempUrl = downloadingFileURL
                
                self.delegate.postVideoDownloaded!(downloadingFileURL)
                
            }
            
            return nil
            
        })
            
        }
        
    }
    
    func formSendToMS(_ viewController: UIViewController){
        
        if sendToMS{
            
            self.vc = viewController
            
            let form = FormController()
            form.post = self
            form.vcPrev = viewController as! MapController
            vc!.navigationController?.pushViewController(form, animated: true)
            
            
            
        }else{
            
            self.delegate.sendedToMS!(self)
            
        }
        

        
    }
    
    var alert: UIAlertController!
    
    func uploadFacebook(_ viewController: UIViewController){
        
        self.vc = viewController
        
        
        if shareOnFacebook {
            
            self.postToFacebook()
        }else{
            
            
            if self.delegate.postedOnFacebook != nil{
                self.delegate.postedOnFacebook!(self)
            }
        }
        
    }
    
    func postToFacebook(){
        
        if publishActions() {
            
            alert = UIAlertController(title: nil, message: "Publicando en Facebook...", preferredStyle: .alert)
            
            alert.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            loadingIndicator.startAnimating()
            
            alert.view.addSubview(loadingIndicator)
            vc!.present(alert, animated: true, completion: nil)
            
            let params: [AnyHashable: Any] = [
                "description": descriptionText,
//                "title": "",
                "file_url": s3Url!
            ]
            
            let request = FBSDKGraphRequest(graphPath: "/me/videos", parameters: params, httpMethod: "POST")
            
            request?.start { (connection: FBSDKGraphRequestConnection!, result: AnyObject!, error:NSError!) in
                
                if error==nil{
                    /*
                    let thankAlert = UIAlertController(title: "¡Gracias por compartirlo!", message: "Se ha publicado exitosamente en tu muro", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    thankAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                        
                        thankAlert.dismissViewControllerAnimated(true, completion: nil)
                        
                    }))
                    
                    self.vc!.presentViewController(thankAlert, animated: true, completion: nil)*/
                    
                    self.alert.dismiss(animated: true, completion: nil)
                    
                    if self.delegate.postedOnFacebook != nil{
                        self.delegate.postedOnFacebook!(self)
                    }
                    
                    
                }else{
                    
                    print(error.localizedDescription)
                    
                    self.alert.dismiss(animated: true, completion: nil)
                    
                    if self.delegate.postedOnFacebookFail != nil{
                        self.delegate.postedOnFacebookFail!(self)
                    }
                    
                }
            
            }
        
        }
        
        
    }
    
    func publishActions() -> Bool{
        
        if FBSDKAccessToken.current() != nil  && FBSDKAccessToken.current().hasGranted("publish_actions"){
            
            print("FBSDKAccessToken.currentAccessToken() != nil  && FBSDKAccessToken.currentAccessToken().hasGranted('publish_actions')")
            
            return true
            
        }else{
            
            print("FBSDKAccessToken.currentAccessToken() == nil  || FBSDKAccessToken.currentAccessToken().hasGranted('publish_actions')")
            
            let loginManager = FBSDKLoginManager()
        
            loginManager.logIn( withPublishPermissions: ["publish_actions"] , from:vc!, handler: { (result: FBSDKLoginManagerLoginResult!, error: NSError!) in
                
                
                if (error != nil) {
                    NSLog(error.localizedDescription)
                    
                    if self.delegate.postedOnFacebookFail != nil{
                        self.delegate.postedOnFacebookFail!(self)
                    }
                    
                } else if result.isCancelled {
                    if self.delegate.postedOnFacebookFail != nil{
                        self.delegate.postedOnFacebookFail!(self)
                    }
                } else if result.grantedPermissions.contains("publish_actions") {
                    
                    self.postToFacebook()
                    
                }
                
            })
            
            return false
            
        }
        
    }
    
}

class Category{
    
    var imageURL:String!
    var description:String!
    var name:String!
    var parseObject:PFObject!
    
    init (){
        
    }
    
    init(_ parseID: String){
        parseObject = PFObject(className:"Category")
    }
    
}
