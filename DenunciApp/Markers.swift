//
//  MarkerView.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 25/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit
import QuartzCore
import GoogleMaps
import CircleProgressBar

class PostMarker: GMSMarker {
    
    var post:Post!
    
    var open = false
    
//    var iconCopy:UIImage!
    
    init(post:Post){
        
        super.init()
        
        self.post = post
        
        self.position = post.location
        
        self.title = post.descriptionText
        
        self.icon = post.getMarker()
        
    }
    
    func hideIcon(){
        self.icon = UIImage()
    }
    
    func showIcon(){
        self.icon = post.getMarker()
    }
    
}

class PostMarkerInfo: UIView {
    @IBOutlet weak var infoView: UIView!
    
    @IBOutlet weak var iconCategory: UIImageView!
    
    @IBOutlet weak var iconMarker: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var circleProgressBar: CircleProgressBar!
    
    @IBOutlet weak var iconMarkerCircle: UIImageView!
    
    var marker: PostMarker?
    
    
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
    }
    
    func setPosting(_ posting:Bool){
        
        circleProgressBar.isHidden = !posting
    }
    
    func postUploadProgress(_ progress: CGFloat) {
        
        circleProgressBar.setProgress(progress, animated: true)
        
    }
    
    func show(){
        
        marker!.hideIcon()
        
        isHidden = false
        
        let scale = CGAffineTransform(scaleX: 0.27, y: 0.27)
        let scale6 = CGAffineTransform(scaleX: 0.6, y: 0.6)
        let down = CGAffineTransform(translationX: 0, y: 50)
        let downmarker = CGAffineTransform(translationX: 0, y: 30)
        let infoTransform = down.concatenating(scale6)
        let markerTransform = downmarker.concatenating(scale)

        iconMarker.transform = markerTransform
        iconMarker.layer.anchorPoint = CGPoint(x: 0.5, y: 1)

        self.infoView.transform = infoTransform
        self.infoView.alpha = 0
        self.iconMarker.alpha = 0
        
        self.iconMarkerCircle.alpha = 1
        self.iconMarkerCircle.transform = markerTransform
        self.iconMarkerCircle.layer.anchorPoint = CGPoint(x: 0.5, y: 1)

        
        UIView.animate(withDuration: 0.3, delay:0.0,
                                   options:UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            
            self.iconMarker.transform = CGAffineTransform.identity
            self.infoView.transform = CGAffineTransform.identity
            self.infoView.alpha = 1
            self.iconMarkerCircle.alpha = 0
            self.iconMarkerCircle.transform = CGAffineTransform.identity
            self.iconMarker.alpha = 1
            
        }) { (Bool) -> Void in
            
            print("hola")
            
        }
        
        
    }
    
    func hide(){
        
        let scale = CGAffineTransform(scaleX: 0.27, y: 0.27)
        let scale6 = CGAffineTransform(scaleX: 0.6, y: 0.6)
        let down = CGAffineTransform(translationX: 0, y: 50)
        let downmarker = CGAffineTransform(translationX: 0, y: 30)
        let infoTransform = down.concatenating(scale6)
        let markerTransform = downmarker.concatenating(scale)
        
        self.iconMarkerCircle.alpha = 0
        self.iconMarkerCircle.transform = CGAffineTransform.identity
        iconMarker.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        self.iconMarkerCircle.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        
        UIView.animate(withDuration: 0.3, delay:0.0,
            options:UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            
            self.iconMarker.transform = markerTransform
            
            self.infoView.transform = infoTransform
            self.infoView.alpha = 0
            self.iconMarker.alpha = 0
            
            self.iconMarkerCircle.alpha = 1
            self.iconMarkerCircle.transform = markerTransform

            
        }) { (Bool) -> Void in
            
                self.marker!.showIcon()
                self.isHidden = true

        }
        
        
    }
    
    
}
