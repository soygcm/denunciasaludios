//
//  CaptureController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 20/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit
import Parse
import FBSDKCoreKit
import FBSDKLoginKit
import CircleProgressBar


class CaptureController: PostSubController {

    @IBOutlet weak var startCaptureButton: UIButton!
    
    @IBOutlet weak var circleProgressBar: CircleProgressBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        step = "capture"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        startCaptureButton.isEnabled = true
        circleProgressBar.setProgress(0, animated: false)
        
    }
    
    // MARK: - IBActions
    
    @IBAction func showMap(_ sender: AnyObject) {
        
        postController.postControllerDelegate.postShowMap(false)
        
    }
    
    
    @IBAction func startCapture(_ sender: AnyObject) {
        
        postController.postControllerDelegate.postStartCapture()
        startCircleProgress()
        startCaptureButton.isEnabled = false
        
    }
    
    var counter = 0.0
    let interval = 0.5
    var duration = 5.0
    var timer = Timer()
    
    func startCircleProgress(){
        
//        timer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: #selector(updateCircle), userInfo: nil, repeats: true)
        
        circleProgressBar.setProgress(1, animated: true, duration: CGFloat(duration))
        
    }
    
    func updateCircle(){
        counter += interval
        let percent = CGFloat(counter/duration)
        
        circleProgressBar.setProgress(percent, animated: true)
        
        if (percent >= 1){
            timer.invalidate()
        }
        
    }
    

}
