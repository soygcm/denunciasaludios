//
//  CategoryController.swift
//  DenunciApp
//
//  Created by Gabriel Castañaza on 20/3/16.
//  Copyright © 2016 GAD. All rights reserved.
//

import UIKit

class CategoryController: PostSubController {
    

    
    
    @IBOutlet weak var categoryDescription: UITextView!
    @IBOutlet weak var face: UIButton!
    
    var categoryIndex = Int()
    
    var currentSelected: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        step = "category"
        
    }
    
    
    func deselectCurrent(){
        
        if currentSelected != nil{
            currentSelected!.isSelected = false
        }
        
    }
    
    func selectCurrent(){
        
        let face = self.postController.post.perception.rawValue
        let imagesList = Post.images[face]!
        let imageCategorySelect = imagesList[categoryIndex]
        
        currentSelected!.isSelected = true
        currentSelected!.setImage(imageCategorySelect, for: .selected)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // MARK: - IBActions
    
    func selectCategory(_ sender: AnyObject, index: Int){
        
        if let button = sender as? UIButton{
            
            postController.category(Post.categories[index])
            categoryIndex = index
            deselectCurrent()
            currentSelected = button
            
            selectCurrent()
            categoryDescription.text = Post.descriptions[categoryIndex]
            
        }
    }
    
    @IBAction func mosquitos(_ sender: AnyObject){
        selectCategory(sender, index:0)
    }
    @IBAction func ruido(_ sender: AnyObject){
        selectCategory(sender, index:1)
    }
    @IBAction func agua(_ sender: AnyObject){
        selectCategory(sender, index:2)
    }
    @IBAction func olores(_ sender: AnyObject){
        selectCategory(sender, index:3)
    }
    @IBAction func residuos(_ sender: AnyObject){
        selectCategory(sender, index:4)
    }
    @IBAction func humo(_ sender: AnyObject){
        selectCategory(sender, index:5)
    }
    @IBAction func vectores(_ sender: AnyObject){
        selectCategory(sender, index:6)
    }
    @IBAction func atencion(_ sender: AnyObject){
        selectCategory(sender, index:7)
    }
    @IBAction func restaurante(_ sender: AnyObject){
        selectCategory(sender, index:8)
    }
    @IBAction func productos(_ sender: AnyObject){
        selectCategory(sender, index:9)
    }
    @IBAction func tabaco(_ sender: AnyObject){
        selectCategory(sender, index:10)
    }
    @IBAction func otros(_ sender: AnyObject){
        selectCategory(sender, index:11)
    }

}
